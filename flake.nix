{
  description = "ARTIQ controller for toptica lasers";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
  inputs.sipyco.url = github:m-labs/sipyco;  
  inputs.sipyco.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, sipyco }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
    in rec {
      packages.x86_64-linux.toptica-lasersdk = pkgs.python3Packages.buildPythonPackage rec {
        pname = "toptica-lasersdk";
        version = "2.7.1";
        src = pkgs.python3Packages.fetchPypi {
          pname = "toptica_lasersdk";
          inherit version;
          sha256 = "3292aed81995e5f64a11f6c388708ffe7f07cc6992a28980754ad350191c9eb3";
        };
        propagatedBuildInputs =  with pkgs.python3Packages; [ ifaddr pyserial ];
      };  

      packages.x86_64-linux = {
        lasersdk-artiq = pkgs.python3Packages.buildPythonPackage {
          pname = "lasersdk-artiq";
          version = "0.2";
          src = self;
          propagatedBuildInputs = [ packages.x86_64-linux.toptica-lasersdk sipyco.packages.x86_64-linux.sipyco ];
        };
      };

      defaultPackage.x86_64-linux = pkgs.python3.withPackages(ps: [ packages.x86_64-linux.lasersdk-artiq ]);

      devShell.x86_64-linux = pkgs.mkShell {
        name = "lasersdk-artiq-dev-shell";
        buildInputs = [
          (pkgs.python3.withPackages(ps: [ sipyco.packages.x86_64-linux.sipyco packages.x86_64-linux.lasersdk-artiq ]))
        ];
      };
    };
}
