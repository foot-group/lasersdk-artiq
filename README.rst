ARTIQ controller for TOPTICA Laser SDK
======================================

Uses https://github.com/quartiq/lasersdk

Example: ::

    aqctl_laser -d 10.0.16.236
    sipyco_rpctool localhost 3272 call get '"system-label"' '"str"'
